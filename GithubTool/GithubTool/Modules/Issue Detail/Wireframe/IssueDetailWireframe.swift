//
//  IssueDetailWireframe.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 17/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation
import UIKit

class IssueDetailWireframe: IssueDetailWireFrameProtocol {
  static func createIssueDetailModule(_ forIssue: Issue) -> UIViewController {
    let view = IssueDetailViewController()
    let presenter: IssueDetailPresenterProtocol & IssueDetailInteractorOutputProtocol = IssueDetailPresenter()
    let interactor: IssueDetailInteractorInputProtocol = IssueDetailInteractor()
    let wireFrame: IssueDetailWireFrameProtocol = IssueDetailWireframe()
    
    view.presenter = presenter
    presenter.view = view
    presenter.issue = forIssue
    presenter.wireFrame = wireFrame
    presenter.interactor = interactor
    interactor.presenter = presenter
    return view
  }
  
  
}
