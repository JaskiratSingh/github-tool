//
//  IssueDetailViewController.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 17/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import UIKit

enum ViewLayers: Int {
  case issue
  case comments
  case count
}

class IssueDetailViewController: UIViewController {

  var presenter: IssueDetailPresenterProtocol?
 
  @IBOutlet weak var tableView: UITableView!
  var comments: [Comment]!
  var issue: Issue!
  private let refreshControl = UIRefreshControl()
  
  
  
    override func viewDidLoad() {
        super.viewDidLoad()

      self.presenter?.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    
  @objc func refresh(_ sender: UIRefreshControl) {
    
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IssueDetailViewController: IssueDetailViewProtocol {
  
  func setupView(_ issue: Issue) {
    self.issue = issue
    self.comments = []
    self.tableView.register(UINib.init(nibName: Nibs.IssueTableViewCell.rawValue, bundle: Bundle.main), forCellReuseIdentifier: Nibs.IssueTableViewCell.rawValue)
    self.tableView.estimatedRowHeight = 50
    self.tableView.rowHeight = UITableView.automaticDimension
    refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
    self.tableView.refreshControl = refreshControl
    self.tableView.dataSource = self
    self.tableView.delegate = self
    self.tableView.reloadData()
    DispatchQueue.main.async {
      self.refreshControl.beginRefreshing()
    }
  }
  
  func setupData(_ comment: [Comment]) {
    self.comments = comment
    DispatchQueue.main.async {
      self.refreshControl.endRefreshing()
      self.tableView.reloadData()
    }
  }
  
  func showAlert(_ value: String) {
    DispatchQueue.main.async {
      self.refreshControl.endRefreshing()
    }
    let AlertController = UIAlertController.init(title: "Error!", message: value, preferredStyle: .alert)
    let AlertActionOk = UIAlertAction.init(title: "OK", style: .default, handler: nil)
    AlertController.addAction(AlertActionOk)
    DispatchQueue.main.async {
      self.present(AlertController, animated: true, completion: nil)
    }
  }
  
  
}

extension IssueDetailViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return ViewLayers.count.rawValue
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch ViewLayers.init(rawValue: section)! {
    case .issue:
      return 1
      
    case .comments:
      return self.comments.count
    
    default:
      return 0
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: Nibs.IssueTableViewCell.rawValue) as? IssueTableViewCell else {
      fatalError(Nibs.IssueTableViewCell.rawValue + "not registered")
    }
    
    cell.selectionStyle = .none
    
    switch ViewLayers.init(rawValue: indexPath.section)! {
    case .issue:
      cell.setIssue(self.issue)
    case .comments:
      cell.setData(self.comments[indexPath.row])
    default:
      break
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    switch ViewLayers.init(rawValue: section)! {
    case .issue:
      return "Issue"
    case .comments: return "Comments"
    default: return ""
    }
  }
  
  
}
