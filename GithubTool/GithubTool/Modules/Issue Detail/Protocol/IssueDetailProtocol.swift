//
//  IssueDetailProtocol.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 17/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation
import UIKit

protocol IssueDetailViewProtocol: class {
  var presenter: IssueDetailPresenterProtocol? { get set }
  func setupView(_ issue: Issue)
  func setupData(_ comment: [Comment])
  func showAlert(_ value: String)
}

protocol IssueDetailWireFrameProtocol: class {
  static func createIssueDetailModule(_ forIssue: Issue) -> UIViewController
}

protocol IssueDetailPresenterProtocol: class {
  var view: IssueDetailViewProtocol? { get set }
  var interactor: IssueDetailInteractorInputProtocol? { get set }
  var wireFrame: IssueDetailWireFrameProtocol? { get set }
  var issue: Issue! {get set}
  func viewDidLoad()
  
}

protocol IssueDetailInteractorOutputProtocol: class {
  func onSuccess(_ comments: [Comment])
  func onError(_ value: String)
}

protocol IssueDetailInteractorInputProtocol: class {
  var presenter: IssueDetailInteractorOutputProtocol? { get set }
  func fetchComments(_ issue: Issue)
  
  
}
