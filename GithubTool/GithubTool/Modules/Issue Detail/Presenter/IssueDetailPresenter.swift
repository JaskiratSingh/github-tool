//
//  IssueDetailPresenter.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 17/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation

class IssueDetailPresenter: IssueDetailPresenterProtocol {
  var issue: Issue!
  
  weak var view: IssueDetailViewProtocol?
  
  var interactor: IssueDetailInteractorInputProtocol?
  
  var wireFrame: IssueDetailWireFrameProtocol?
  
  func viewDidLoad() {
    self.view?.setupView(self.issue)
    self.interactor?.fetchComments(self.issue)
  }
  
  
}

extension IssueDetailPresenter: IssueDetailInteractorOutputProtocol {
  func onSuccess(_ comments: [Comment]) {
    self.view?.setupData(comments)
  }
  
  func onError(_ value: String) {
    self.view?.showAlert(value)
  }
  
  
}
