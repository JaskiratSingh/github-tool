//
//  IssueDetailInteractor.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 17/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation
import Alamofire

class IssueDetailInteractor: IssueDetailInteractorInputProtocol {
  weak var presenter: IssueDetailInteractorOutputProtocol?
  
  func fetchComments(_ issue: Issue) {
    
    let commentsFromDb = CoreDataManager.shared.getCommentsFor(issue)
    
    if commentsFromDb.count != 0 {
      self.presenter?.onSuccess(commentsFromDb)
    } else {
      let Params: [String: Any] = [
                                   ParamKeys.sort.rawValue: GithubApiConstant.sort.rawValue,
                                   ParamKeys.direction.rawValue: GithubApiConstant.direction.rawValue
      ]
      guard let url = issue.commentsUrl else { self.presenter?.onError("No Url"); return }
      request(url, method: HTTPMethod.get, parameters: Params, encoding:  URLEncoding.default, headers: nil)
        .responseJSON(queue: nil, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
          
          
          switch response.result {
          case .success(_):  //save to core data
          if let result = response.result.value as? [[String: Any]]{
            let comments =  CoreDataManager.shared.updateCommentsFor(issue, comments: CommentResponseData.getArray(result))
            self.presenter?.onSuccess(comments)
            }
          case .failure(let error):
            self.presenter?.onError(error.localizedDescription)
            print(error.localizedDescription)
            
          }
      }
    }
    
    
  }
  
  
}
