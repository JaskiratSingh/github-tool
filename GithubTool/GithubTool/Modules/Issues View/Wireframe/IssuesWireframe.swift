//
//  IssuesWireframe.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 16/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation
import UIKit

class IssuesWireframe: IssuesWireFrameProtocol {
  func moveToDetailScreen(_ from: IssuesViewProtocol, with Issue: Issue) {
    
    let DetailView = IssueDetailWireframe.createIssueDetailModule(Issue)
    
    if let sourceView = from as? UIViewController {
      DispatchQueue.main.async {
        sourceView.navigationController?.pushViewController(DetailView, animated: true)
      }
    }
  }
  
  static func createIssuesModule() -> UIViewController {
    let view = IssuesViewController()
    let presenter: IssuesPresenterProtocol & IssuesInteractorOutputProtocol = IssuesPresenter()
    let interactor: IssuesInteractorInputProtocol = IssuesInteractor()
    let wireFrame: IssuesWireFrameProtocol = IssuesWireframe()
    
    view.presenter = presenter
    presenter.view = view
    presenter.wireFrame = wireFrame
    presenter.interactor = interactor
    interactor.presenter = presenter
    return view
  }
  
}
