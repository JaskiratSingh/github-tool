//
//  IssueTableViewCell.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 17/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import UIKit

class IssueTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func setData(_ from: Issue) {
    self.textLabel?.text = from.title
    
    if let text = from.body, !text.isEmpty {
      let index = text.index(text.startIndex, offsetBy: min(139, text.count-1))
      self.detailTextLabel?.text = String(text[...index]) + (text.count > 140 ? "..." : "" )
    } else {
      self.detailTextLabel?.text = ""
    }
    
  }
  
  func setData(_ from: Comment) {
    self.textLabel?.text = from.commentar_name ?? ""
    
    if let text = from.body, !text.isEmpty {
      self.detailTextLabel?.text = text
    } else {
      self.detailTextLabel?.text = ""
    }
  }
  
  func setIssue(_ from: Issue) {
    
    self.textLabel?.text = from.title
   
    if let text = from.body, !text.isEmpty {
      self.detailTextLabel?.text = text
    } else {
      self.detailTextLabel?.text = ""
    }
  }
    
}
