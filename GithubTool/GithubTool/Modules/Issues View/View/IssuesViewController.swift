//
//  IssuesViewController.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 16/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import UIKit

class IssuesViewController: UIViewController {

  var presenter: IssuesPresenterProtocol?
  @IBOutlet weak var tableView: UITableView!
  var issues: [Issue]!
  private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
     self.setupUI()
      self.presenter?.viewDidLoad()
        // Do any additional setup after loading the view.
    }
  
  func setupUI() {
    self.navigationItem.title = "Issues"
    self.tableView.register(UINib.init(nibName: Nibs.IssueTableViewCell.rawValue, bundle: Bundle.main), forCellReuseIdentifier: Nibs.IssueTableViewCell.rawValue)
    self.tableView.estimatedRowHeight = 50
    self.tableView.rowHeight = UITableView.automaticDimension
    refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
    self.tableView.refreshControl = refreshControl
    self.tableView.dataSource = self
    self.tableView.delegate = self
    
    DispatchQueue.main.async {
      self.tableView.contentOffset = CGPoint(x: 0, y: -self.refreshControl.frame.size.height)
      self.refreshControl.beginRefreshing()
    }

  }

  
  @objc func refresh(_ sender: UIRefreshControl) {
    
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IssuesViewController: IssuesViewProtocol {
  
  func setupData(_ issues: [Issue]) {
    self.issues = issues
    DispatchQueue.main.async {
      self.refreshControl.endRefreshing()
      self.tableView.reloadData()
    }
  }
  
  func showAlert(_ value: String) {
    
    DispatchQueue.main.async {
      self.refreshControl.endRefreshing()
    }
    
    let AlertController = UIAlertController.init(title: "Error!", message: value, preferredStyle: .alert)
    let AlertActionOk = UIAlertAction.init(title: "OK", style: .default, handler: nil)
    AlertController.addAction(AlertActionOk)
    DispatchQueue.main.async {
      self.present(AlertController, animated: true, completion: nil)
    }
  }
  
}

extension IssuesViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let issues = self.issues else {
      return 0
    }
    
    return issues.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: Nibs.IssueTableViewCell.rawValue) as? IssueTableViewCell else {
      fatalError(Nibs.IssueTableViewCell.rawValue + "not registered")
    }
    cell.setData(self.issues[indexPath.row])
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    self.presenter?.didTapAt(self.issues[indexPath.row])
  }
}
