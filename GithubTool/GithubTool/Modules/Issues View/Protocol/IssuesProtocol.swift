//
//  IssuesProtocol.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 16/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation
import UIKit

protocol IssuesViewProtocol: class {
  var presenter: IssuesPresenterProtocol? { get set }
  func setupData(_ issues: [Issue])
  func showAlert(_ value: String)
}

protocol IssuesWireFrameProtocol: class {
  static func createIssuesModule() -> UIViewController
  func moveToDetailScreen(_ from: IssuesViewProtocol, with Issue: Issue)
}

protocol IssuesPresenterProtocol: class {
  var view: IssuesViewProtocol? { get set }
  var interactor: IssuesInteractorInputProtocol? { get set }
  var wireFrame: IssuesWireFrameProtocol? { get set }
  func viewDidLoad()
  func didTapAt(_ issue: Issue)
}

protocol IssuesInteractorOutputProtocol: class {
  func onSuccess(_ issues: [Issue])
  func onError(_ value: String)
}

protocol IssuesInteractorInputProtocol: class {
  var presenter: IssuesInteractorOutputProtocol? { get set }
  func fetchOpenIssue()
  
  
}
