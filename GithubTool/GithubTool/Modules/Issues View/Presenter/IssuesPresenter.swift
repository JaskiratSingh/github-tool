//
//  IssuesPresenter.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 16/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation

class IssuesPresenter: IssuesPresenterProtocol {

  weak var view: IssuesViewProtocol?
  var interactor: IssuesInteractorInputProtocol?  
  var wireFrame: IssuesWireFrameProtocol?
  
  func viewDidLoad() {
    self.interactor?.fetchOpenIssue()
  }
  
  func didTapAt(_ issue: Issue) {
    guard  issue.comment_count > 0  else {
      self.onError("No comments for the post")
      return
    }
    
    self.wireFrame?.moveToDetailScreen(self.view!, with: issue)
    
  }
  
}

extension IssuesPresenter: IssuesInteractorOutputProtocol {
  func onSuccess(_ issues: [Issue]) {
    self.view?.setupData(issues)
  }
  
  func onError(_ value: String) {
    self.view?.showAlert(value)
  }
}
