//
//  IssuesInteractor.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 16/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

class IssuesInteractor: IssuesInteractorInputProtocol {
  weak var presenter: IssuesInteractorOutputProtocol?
  func fetchOpenIssue() {
    
    let IssueList = CoreDataManager.shared.getIssueFromDb()
    
    if IssueList.count != 0 {
      self.presenter?.onSuccess(IssueList)
    } else {
      let Params: [String: Any] = [ParamKeys.state.rawValue: GithubApiConstant.state.rawValue,
                                   ParamKeys.sort.rawValue: GithubApiConstant.sort.rawValue,
                                   ParamKeys.direction.rawValue: GithubApiConstant.direction.rawValue
      ]
      request(Urls.githubIssues.rawValue, method: HTTPMethod.get, parameters: Params, encoding:  URLEncoding.default, headers: nil)
        .responseJSON(queue: nil, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
          
          switch response.result {
          case .success(_):  //save to core data
            if let result = response.result.value as? [[String: Any]]{
              let Arr = IssueResponseData.getArray(result)
              self.presenter?.onSuccess(CoreDataManager.shared.saveIssuesToDb(Arr))
            }
          case .failure(let error):
            print(error.localizedDescription)
            
          }
      }
    }
  }
  
}
