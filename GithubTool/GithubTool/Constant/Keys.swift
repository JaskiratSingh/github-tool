//
//  Keys.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 16/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation
import UIKit
import CoreData

enum Storyboard: String {
  case Main
  
  func get() -> UIStoryboard {
    switch self {
    case .Main:
      return UIStoryboard.init(name: self.rawValue, bundle: Bundle.main)
      
    }
  }
}

enum Nibs: String {
  case IssueTableViewCell
}

enum Urls: String {
  case githubIssues = "https://api.github.com/repos/firebase/firebase-ios-sdk/issues"
}

enum ParamKeys: String {
  case state
  case sort
  case direction
  case per_page
  case page
}

enum GithubApiConstant: String {
  case state = "open"
  case sort = "updated"
  case direction = "dsc"
  
}


extension String {
  
  func toDate(withFormat format: String = "yyyy-MM-dd'T'HH:mm:ssZ")-> Date?{
    
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current
    dateFormatter.dateFormat = format
    let date = dateFormatter.date(from: self)
    return date
    
  }
}

extension Date {
  
  func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
    
    let currentCalendar = Calendar.current
    
    guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
    guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
    
    return end - start
  }
}

