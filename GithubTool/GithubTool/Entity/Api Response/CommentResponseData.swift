//
//	Comment.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CommentResponseData {

	var body : String!
	var user : String!
  var updated_at: String!
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
  
  static func getArray(_ from: [[String: Any]]) -> [CommentResponseData] {
    return from.map({ return CommentResponseData.init(fromDictionary: $0)})
  }
  
	init(fromDictionary dictionary: [String:Any]){
		body = dictionary["body"] as? String
		if let userData = dictionary["user"] as? [String:Any]{
				let userObj = User(fromDictionary: userData)
                self.user = userObj.login
			}
      updated_at = dictionary["updated_at"] as? String
	}

	
}
