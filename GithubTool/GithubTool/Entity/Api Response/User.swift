//
//	User.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct User{

	var login : String!
	
	init(fromDictionary dictionary: [String:Any]){
		login = dictionary["login"] as? String
	}

	
}
