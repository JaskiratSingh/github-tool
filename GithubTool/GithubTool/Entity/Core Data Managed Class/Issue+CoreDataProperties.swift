//
//  Issue+CoreDataProperties.swift
//  
//
//  Created by Jaskirat Singh on 19/04/19.
//
//

import Foundation
import CoreData


extension Issue {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Issue> {
        return NSFetchRequest<Issue>(entityName: "Issue")
    }

    @NSManaged public var body: String?
    @NSManaged public var comment_count: Int32
    @NSManaged public var commentsUrl: String?
    @NSManaged public var title: String?
    @NSManaged public var updated_at: Date?
    @NSManaged public var created_at: Date?
    @NSManaged public var id: Int32
    @NSManaged public var comments: NSSet?

}

// MARK: Generated accessors for comments
extension Issue {

    @objc(addCommentsObject:)
    @NSManaged public func addToComments(_ value: Comment)

    @objc(removeCommentsObject:)
    @NSManaged public func removeFromComments(_ value: Comment)

    @objc(addComments:)
    @NSManaged public func addToComments(_ values: NSSet)

    @objc(removeComments:)
    @NSManaged public func removeFromComments(_ values: NSSet)

}
