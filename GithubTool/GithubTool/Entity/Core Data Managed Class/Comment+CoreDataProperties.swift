//
//  Comment+CoreDataProperties.swift
//  
//
//  Created by Jaskirat Singh on 19/04/19.
//
//

import Foundation
import CoreData


extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment")
    }

    @NSManaged public var commentar_name: String?
    @NSManaged public var body: String?
    @NSManaged public var issue: Issue?
    @NSManaged public var updated_at: Date?

}
