//
//  CoreDataManager.swift
//  GithubTool
//
//  Created by Jaskirat Singh on 19/04/19.
//  Copyright © 2019 Jaskirat Singh. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager: NSObject {
  static let shared = CoreDataManager()
  
  enum Tables: Int {
    case Issue
    case Comment
    
    func getRelationship() -> [String] {
      switch self {
      case .Issue:
        return ["comments"]
      default:
        return []
      }
    }
  }
  
  var context: NSManagedObjectContext?{
    return (UIApplication.shared.delegate as? AppDelegate)?
      .persistentContainer.viewContext
  }
  
  func saveContext() throws {
    guard let appContext = self.context else {
      return
    }
    
    if appContext.hasChanges {
      try appContext.save()
    }
  }
  
  func cleanDb() {
    guard let ContextObj = self.context else { return  }

    let IssueFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Issue")
    let request = NSBatchDeleteRequest(fetchRequest: IssueFetchRequest)
    
    let CommentFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Comment")
    let Commentrequest = NSBatchDeleteRequest(fetchRequest: CommentFetchRequest)
    
    do {
      try ContextObj.execute(request)
      try ContextObj.execute(Commentrequest)
      try ContextObj.save()
    } catch {
      print ("There was an error")
    }
  }
  
  func saveIssuesToDb(_ values: [IssueResponseData]) -> [Issue] {
    guard let ContextObj = self.context else { return [] }
    let DateVal = Date()
    let Issues = values.map { (element) -> Issue in
      let IssueObj = Issue(context: ContextObj)
      IssueObj.body = element.body
      IssueObj.id = Int32.init(element.id!)
      IssueObj.comment_count = Int32.init(element.comments)
      IssueObj.commentsUrl = element.commentsUrl
      IssueObj.title = element.title
      IssueObj.updated_at = element.updatedAt.toDate()
      IssueObj.created_at = DateVal
      return IssueObj
    }
    
    do {
      try saveContext()
    } catch {
      print("error saving")
      
    }
    
    return Issues
    
  }
  
  func checkLastUpdatedDbShouldUpdateData() -> Bool {
    guard let ContextObj = self.context else {
      return true
    }
    let IssueFetchRequest: NSFetchRequest<Issue> = Issue.fetchRequest()
    IssueFetchRequest.fetchLimit = 1
    let Issues = try! ContextObj.fetch(IssueFetchRequest)
    guard let Issue = Issues.first,
      Issues.count == IssueFetchRequest.fetchLimit else {
        return true
    }
    
    if Date().interval(ofComponent: .hour, fromDate: Issue.created_at!) >= 24 {
      return true
    } else {
      return false
    }

  }
  
  func getIssueFromDb() -> [Issue] {
    
    if checkLastUpdatedDbShouldUpdateData() {
      cleanDb()
    }
    
     guard let ContextObj = self.context else { return [] }
    let IssueFetchRequest: NSFetchRequest<Issue> = Issue.fetchRequest()
    let sort = NSSortDescriptor(key: "updated_at", ascending: false)
    IssueFetchRequest.sortDescriptors = [sort]
    let Issues = try! ContextObj.fetch(IssueFetchRequest)
    return Issues
  }
  
  func updateCommentsFor(_ issue: Issue, comments: [CommentResponseData]) -> [Comment] {
    guard let ContextObj = self.context else { return [] }
    
    let commentArr = comments.map { (element) -> Comment in
      let CommentObj = Comment(context: ContextObj)
      CommentObj.body = element.body
      CommentObj.commentar_name = element.user
      CommentObj.updated_at = element.updated_at.toDate()
      return CommentObj
    }
    issue.comments = NSSet(array: commentArr)
    
    do {
      try saveContext()
    } catch {
      print("error saving")
      
    }

    return commentArr
  }
  
  func getCommentsFor(_ from: Issue) -> [Comment] {
    guard let ContextObj = self.context else { return [] }
    let IssueFetchRequest: NSFetchRequest<Issue> = Issue.fetchRequest()
    IssueFetchRequest.predicate = NSPredicate(format: "id == %@" ,
                                    argumentArray: [from.id])
    IssueFetchRequest.relationshipKeyPathsForPrefetching = ["comments"]

    let Issues = try! ContextObj.fetch(IssueFetchRequest)
    guard Issues.count == 1 else {
      print("Duplicate Present")
      return []
    }
    return Issues.first!.comments?.allObjects.sorted(by: { (element1, element2) -> Bool in
      guard let first = element1 as? Comment , let second = element2 as? Comment else { return false }
      
      return first.updated_at! > second.updated_at!
      }) as? [Comment] ?? []
  }
  
}
