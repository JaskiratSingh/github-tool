//
//	Issue.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct IssueResponseData {
  
  var body : String!
  var comments : Int!
  var commentsUrl : String!
  var title : String!
  var updatedAt : String!
  var id: Int!
  
  /**
   * Instantiate the instance using the passed dictionary values to set the properties values
   */
  static func getArray(_ from: [[String: Any]]) -> [IssueResponseData] {
    return from.map({ return IssueResponseData.init(fromDictionary: $0)})
  }
  
  init(fromDictionary dictionary: [String:Any]){
    body = dictionary["body"] as? String
    comments = dictionary["comments"] as? Int
    commentsUrl = dictionary["comments_url"] as? String
    title = dictionary["title"] as? String
    updatedAt = dictionary["updated_at"] as? String
    id = dictionary["id"] as? Int
  }
  
  
  
  
  
  
}
